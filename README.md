# magellan
NCA Burial Operations and Cemetery Management

### Project Overview
Magellan provides benefits administration, burial operations, and facilities management in a modern Web interface. Based on the legacy system feature set and data model, NCA Magellan supports both desktop and mobile access.

The client side technology stack consists of HTML5, CSS3, and JavaScript. The server side is comprised of Linux, Apache, MySQL, and PHP (LAMP). Twitter Bootstrap and jQuery are also used.

Architecturally, Magellan is organized around several core capabilities, or subsystems:

- Interment Case Management
- Interment Scheduling
- Monument Case Management
- Gravesite Management
- User Account Management
- Reporting
- Online Help

### Local Environment Setup
The core Magellan capabilities rely on the LAMP stack: Linux, Apache, MySQL, and PHP. These servers must be running as applications or services. This section provides installation, setup, and configuration information for running Magellan on a local machine. These instructions cover MacOS based machines, such as a MacBook Pro. Windows based machines should follow similar steps, using a Windows/Apache/MySQL/ PHP/Perl tool, such as Apache Friends XAMPP.

#### Linux (MacOS local machine)
MacOS Montery (12.x) comes with Apache 2.4.15 preinstalled. However, Apple has discontinued native support for PHP. Given this, I beleive the best approach for installing Apache + PHP + MariaDB is via the XAMPP all-in-one distribution, which you can find at: https://www.apachefriends.org/index.html.  

Download and install the latest version of XAMPP for MacOS by following the link on the opening page. For my MBP installation, I had trouble using the links on the main page. The files were oddly formed archives and did not extract as expected. To get around this, I clicked the "Download" menu link at the top of the page, scrolled down to the Mac OS X section, clicked "More Downloads", selected XAMPP Mac OS X, and selected 8.1.1.

Frome here, I slected:
> xampp-osx-8.1.1-2-installer.dmg

The downloaded "dmg" file installs XAMPP to /Applications/XAMPP. From there, the XAMPP Control app can be used to start and stop the desired services. The installed Apache HTTPD service uses the following directory as the default httpdoc location:
> /Applications/XAMPP/xamppfiles/htpdocs/

This folder can be used as the base directory for your application, or you may want to create a subfolder as the location for your web app files and subdirectories, e.g.:
> /Applications/XAMPP/xamppfiles/htpdocs/magellan

The installed XAMPP administration app allows you to start and stop the LAMP servers. To launch the XAMPP administration app, run:
> manager-osx

#### Apache HTTPD Web Server Setup
