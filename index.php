<?php
	session_start();
	session_destroy();
?>

<!DOCTYPE html>
<!-- Magellan Landing and Signin Screen -->

<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>NCA Magellan | Sign In</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta name="description" content="">

    <!-- Le styles -->
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="./bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Inline styling that maybe should be in a stylesheet -->
    <style type="text/css">
      body
      {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: white;
      }
      .form-signin
      {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox
      {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"]
      {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../bootstrap/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <!-- double check the href path... the current path doesn't exist -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>   
    <!-- ********** Main Area ********** -->
  	<div class="container">
	  	<div class="row">
  			<div class="span12">
  				<div class="well">
			  		<h1>NCA Magellan</h1>
  					This is a restricted website for authorized use only.
  					<br /><br />
  		
			  		<!-- ********** Signin Form ********** -->
			      <form class="form-signin" method="post" action="validate.php">
			        <h2 class="form-signin-heading">Please sign in</h2>
			        <label>Name
	      			  <input type="text" name="user-name" class="input-block-level" autofocus>
      			  </label>
							<label>Password
				        <input type="password" name="user-password" class="input-block-level">
				      </label>
      			  <label class="checkbox">
			          <input type="checkbox" value="remember-me"> Remember me
      			  </label>
							<input class="btn btn-primary" type="submit" name="submit" value="Sign In">
							<a href="./about/" class="btn btn-link">Learn More</a>
      			</form>
			    </div> <!-- well -->
		    </div> <!-- span12 -->
    	</div> <!-- row -->
    </div> <!-- /container -->

		<div class="container">
			<br />
			<footer>
				<?php
					print("<p style='color:darkslategray;font-size:small'>Copyright (c) 2013 by Chuck Sylvester</p>");
				?>
			</footer>
		</div>
  </body>
</html>